﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProductInventorySystem.Models
{
    public class Product
    {
        public int productid { get; set; }
        [Required]
        public string name { get; set; }
        [Required]
        public string description { get; set; }
        public double price { get; set; }
        [Range(0, 999)]
        public int qty { get; set; }
        [Required]
        public string active { get; set; }

    }
}