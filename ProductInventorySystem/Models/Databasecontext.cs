﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProductInventorySystem.Models
{
    public class Databasecontext : DbContext
    {
        public Databasecontext() : base("dbconnection")
        {

        }
        public DbSet<Product> Products { get; set; }
    }
}