namespace ProductInventorySystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        productid = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        description = c.String(),
                        price = c.Double(nullable: false),
                        qty = c.Int(nullable: false),
                        active = c.String(),
                    })
                .PrimaryKey(t => t.productid);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Products");
        }
    }
}
